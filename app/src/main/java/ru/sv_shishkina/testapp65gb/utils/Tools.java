package ru.sv_shishkina.testapp65gb.utils;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Tools {
    public static String getAge(String birthday) {
        long age = 0;
        String birthdayYear = "";

        SimpleDateFormat sdf  = new SimpleDateFormat("yyyy", Locale.getDefault());
        String currentYear = sdf.format(new Date());

        String [] dateParts = birthday.split("\\-");

        for (String datePart : dateParts) {
            if (datePart.toCharArray().length == 4) {
                birthdayYear = datePart;
                break;
            }
        }

        if (!birthdayYear.isEmpty()) {
            age = Long.valueOf(currentYear) - Long.valueOf(birthdayYear);
        }

        return String.valueOf(age);
    }

    public static String normalizeName(String nameTemplate, String firstName, String lastName) {
        String newFName;
        String newLName;

        StringBuilder fName = new StringBuilder(firstName.toLowerCase());
        fName.setCharAt(0, Character.toUpperCase(fName.charAt(0)));
        newFName = fName.toString();

        StringBuilder lName = new StringBuilder(lastName.toLowerCase());
        lName.setCharAt(0, Character.toUpperCase(lName.charAt(0)));
        newLName = lName.toString();

        return String.format(nameTemplate, newFName, newLName);
    }
}
