package ru.sv_shishkina.testapp65gb.utils.navigator;


import android.support.v4.app.Fragment;

public interface NavigationHandler {
    void replaceWithBackStack(Fragment fragment, String backStack);
}
