package ru.sv_shishkina.testapp65gb.utils;


import android.os.Bundle;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import icepick.Bundler;

public class BaseListBundler<T extends Parcelable> implements Bundler<List<T>> {
    @Override
    public void put(String s, List<T> data, Bundle bundle) {
        if(null != data)
            bundle.putParcelableArrayList(s, new ArrayList<T>(data));
    }

    @Override
    public List<T> get(String s, Bundle bundle) {
        return bundle.getParcelableArrayList(s);
    }
}
