package ru.sv_shishkina.testapp65gb.domain.repositories;


import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.datasource.local.DBDataSource;
import ru.sv_shishkina.testapp65gb.domain.exceptions.EmtyDBException;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.NetworkDataSource;
import ru.sv_shishkina.testapp65gb.domain.exceptions.NetworkException;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

public class EmployeesRepositoryImpl implements EmployeesRepository {
    final private NetworkDataSource networkDataSource;
    final private DBDataSource dbDataSource;

    public EmployeesRepositoryImpl(NetworkDataSource networkDataSource, DBDataSource dbDataSource) {
        this.networkDataSource = networkDataSource;
        this.dbDataSource = dbDataSource;
    }

    @Override
    public List<Specialty> getSpecialties() throws NetworkException, EmtyDBException {
        List<Specialty> specialties;

        List<Employee> employees = networkDataSource.getEmployeesFromNetwork();
        dbDataSource.updateEmployees(employees);
        specialties = dbDataSource.getSpecialties();

        return specialties;
    }

    @Override
    public List<Employee> getEmployees(Long specialtyId) throws EmtyDBException {
        return dbDataSource.getEmployeesBySpecialty(specialtyId);
    }

    @Override
    public Employee getEmployee(Long id) {
        return dbDataSource.getEmployeeById(id);
    }
}
