package ru.sv_shishkina.testapp65gb.domain.repositories;


import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;
import ru.sv_shishkina.testapp65gb.domain.exceptions.EmtyDBException;
import ru.sv_shishkina.testapp65gb.domain.exceptions.NetworkException;

public interface EmployeesRepository {
    List<Specialty> getSpecialties() throws NetworkException, EmtyDBException;
    List<Employee> getEmployees(Long specialtyId) throws EmtyDBException;
    Employee getEmployee(Long id);
}
