package ru.sv_shishkina.testapp65gb.domain.datasource.network.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Specialty implements Parcelable {
    @SerializedName("specialty_id")
    private long id;
    @SerializedName("name")
    private String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
    }

    public Specialty() {
    }

    protected Specialty(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
    }

    public static final Parcelable.Creator<Specialty> CREATOR = new Parcelable.Creator<Specialty>() {
        @Override
        public Specialty createFromParcel(Parcel source) {
            return new Specialty(source);
        }

        @Override
        public Specialty[] newArray(int size) {
            return new Specialty[size];
        }
    };
}
