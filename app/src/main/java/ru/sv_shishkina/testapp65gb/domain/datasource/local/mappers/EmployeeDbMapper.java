package ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers;


import java.util.ArrayList;
import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBEmployee;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBSpecialty;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

public class EmployeeDbMapper implements Mapper<Employee, DBEmployee> {
    private final Mapper<Specialty, DBSpecialty> specialtyDBMapper;

    public EmployeeDbMapper(Mapper<Specialty, DBSpecialty> specialtyDBMapper) {
        this.specialtyDBMapper = specialtyDBMapper;
    }

    @Override
    public Employee dataToModel(DBEmployee dbEmployee) {

        final Employee employee = new Employee();

        employee.setId(dbEmployee.getId());
        employee.setFirstName(dbEmployee.getFirstName());
        employee.setLastName(dbEmployee.getLastName());
        employee.setBirthday(dbEmployee.getBirthday());
        employee.setPhoto(dbEmployee.getPhoto());

        List<Specialty> specialties = new ArrayList<>();
        for (DBSpecialty dbSpecialty : dbEmployee.getSpecialties()) {
            specialties.add(specialtyDBMapper.dataToModel(dbSpecialty));
        }
        employee.setSpecialties(specialties);

        return employee;
    }

    @Override
    public DBEmployee modelToData(Employee employee) {
        final DBEmployee dbEmployee = new DBEmployee();

        dbEmployee.setFirstName(employee.getFirstName());
        dbEmployee.setLastName(employee.getLastName());
        dbEmployee.setBirthday(employee.getBirthday());
        dbEmployee.setPhoto(employee.getPhoto());

        List<DBSpecialty> dbSpecialties = new ArrayList<>();
        for (Specialty specialty : employee.getSpecialties()) {
            dbSpecialties.add(specialtyDBMapper.modelToData(specialty));
        }
        dbEmployee.setSpecialties(dbSpecialties);

        return dbEmployee;
    }
}
