package ru.sv_shishkina.testapp65gb.domain;


public class OperationResponse<ResultType> {
    private Boolean isSuccessful;
    private ResultType result;
    private Throwable error;

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }

    public ResultType getResult() {
        return result;
    }

    public void setResult(ResultType result) {
        this.result = result;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }
}
