package ru.sv_shishkina.testapp65gb.domain.datasource.local;


import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.exceptions.EmtyDBException;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

public interface DBDataSource {
    void updateEmployees(List<Employee> employees);
    List<Specialty> getSpecialties() throws EmtyDBException;
    List<Employee> getEmployeesBySpecialty(Long specialtyId) throws EmtyDBException;
    Employee getEmployeeById(Long id);
}
