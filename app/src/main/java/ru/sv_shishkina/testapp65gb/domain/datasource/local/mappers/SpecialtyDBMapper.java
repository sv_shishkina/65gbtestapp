package ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers;


import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBSpecialty;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

public class SpecialtyDBMapper implements Mapper<Specialty, DBSpecialty> {

    @Override
    public Specialty dataToModel(DBSpecialty dbSpecialty) {
        final Specialty specialty = new Specialty();

        specialty.setId(dbSpecialty.getId());
        specialty.setTitle(dbSpecialty.getTitle());

        return specialty;
    }

    @Override
    public DBSpecialty modelToData(Specialty specialty) {
        final DBSpecialty dbSpecialty = new DBSpecialty();

        dbSpecialty.setId(specialty.getId());
        dbSpecialty.setTitle(specialty.getTitle());

        return dbSpecialty;
    }
}
