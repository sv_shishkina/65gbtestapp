package ru.sv_shishkina.testapp65gb.domain.datasource.local.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class DBJoin {
    @Id(autoincrement = true)
    private Long id;
    private Long employeeId;
    private Long specialtyId;
    @Generated(hash = 613534349)
    public DBJoin(Long id, Long employeeId, Long specialtyId) {
        this.id = id;
        this.employeeId = employeeId;
        this.specialtyId = specialtyId;
    }
    @Generated(hash = 600655370)
    public DBJoin() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getEmployeeId() {
        return this.employeeId;
    }
    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
    public Long getSpecialtyId() {
        return this.specialtyId;
    }
    public void setSpecialtyId(Long specialtyId) {
        this.specialtyId = specialtyId;
    }
    
}
