package ru.sv_shishkina.testapp65gb.domain.datasource.local;


import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBEmployeeDao;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBJoin;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBJoinDao;
import ru.sv_shishkina.testapp65gb.domain.exceptions.EmtyDBException;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers.Mapper;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBEmployee;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBSpecialty;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DaoSession;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

public class DBDataSourceImpl implements DBDataSource {
    final private DaoSession daoSession;
    final private Mapper<Employee, DBEmployee> employeeMapper;
    final private Mapper<Specialty, DBSpecialty> specialtyMapper;

    public DBDataSourceImpl(DaoSession daoSession, Mapper<Employee, DBEmployee> employeeMapper,
                            Mapper<Specialty, DBSpecialty> specialtyMapper) {
        this.daoSession = daoSession;
        this.employeeMapper = employeeMapper;
        this.specialtyMapper = specialtyMapper;
    }

    @Override
    public void updateEmployees(List<Employee> employees) {
        daoSession.getDBJoinDao().deleteAll();
        daoSession.getDBEmployeeDao().deleteAll();
        daoSession.getDBSpecialtyDao().deleteAll();

        final List<DBEmployee> dbEmployees = new ArrayList<>();
        final List<DBSpecialty> dbSpecialties = new ArrayList<>();

        for (Employee employee : employees) {
            dbEmployees.add(employeeMapper.modelToData(employee));

            for (Specialty specialty : employee.getSpecialties()) {
                dbSpecialties.add(specialtyMapper.modelToData(specialty));
            }
        }

        daoSession.getDBEmployeeDao().insertOrReplaceInTx(dbEmployees);
        daoSession.getDBSpecialtyDao().insertOrReplaceInTx(dbSpecialties);

        insertToJoinTable();
    }

    @Override
    public List<Specialty> getSpecialties() throws EmtyDBException {
        final List<Specialty> specialties = new ArrayList<>();
        final List<DBSpecialty> dbSpecialties = daoSession.getDBSpecialtyDao().queryBuilder().list();

        for (DBSpecialty dbSpecialty : dbSpecialties) {
            specialties.add(specialtyMapper.dataToModel(dbSpecialty));
        }

        if (specialties.isEmpty()) {
            throw new EmtyDBException();
        }

        return specialties;
    }

    @Override
    public List<Employee> getEmployeesBySpecialty(Long specialtyId) throws EmtyDBException {
        final List<Employee> employees = new ArrayList<>();

        QueryBuilder<DBEmployee> qb = daoSession.getDBEmployeeDao().queryBuilder();
        qb.join(DBEmployeeDao.Properties.Id, DBJoin.class, DBJoinDao.Properties.EmployeeId)
                .where(DBJoinDao.Properties.SpecialtyId.eq(specialtyId));

        List<DBEmployee> dbEmployees = qb.list();
        for (DBEmployee dbEmployee : dbEmployees) {
            employees.add(employeeMapper.dataToModel(dbEmployee));
        }

        if (employees.isEmpty()) {
            throw new EmtyDBException();
        }

        return employees;
    }

    @Override
    public Employee getEmployeeById(Long id) {
        DBEmployee dbEmployee = daoSession.getDBEmployeeDao().queryBuilder().where(DBEmployeeDao.Properties.Id.eq(id)).unique();
        return employeeMapper.dataToModel(dbEmployee);
    }

    private void insertToJoinTable() {
        final List<DBEmployee> dbEmployees = daoSession.getDBEmployeeDao().queryBuilder().list();

        List<DBJoin> dbJoins = new ArrayList<>();
        for (DBEmployee dbEmployee : dbEmployees) {
            for (DBSpecialty dbSpecialty : dbEmployee.getSpecialties()) {
                dbJoins.add(new DBJoin(null, dbEmployee.getId(), dbSpecialty.getId()));
            }
        }

        daoSession.getDBJoinDao().insertOrReplaceInTx(dbJoins);
    }
}
