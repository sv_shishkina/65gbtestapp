package ru.sv_shishkina.testapp65gb.domain.exceptions;



public class EmtyDBException extends Exception {
    public EmtyDBException() {
    }

    public EmtyDBException(Error error) {
        super(error);
    }

    public EmtyDBException(String detailMessage) {
        super(detailMessage);
    }

    public EmtyDBException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public EmtyDBException(Throwable throwable) {
        super(throwable);
    }
}
