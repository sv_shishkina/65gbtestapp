package ru.sv_shishkina.testapp65gb.domain.datasource.network;


import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.dto.NetworkResponse;

public interface Service65Gb {
    String ENDPOINT = "http://65apps.com/";

    @GET("images/testTask.json")
    Call<NetworkResponse> getEmployees();
}
