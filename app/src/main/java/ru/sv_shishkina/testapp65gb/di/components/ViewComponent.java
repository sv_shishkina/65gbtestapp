package ru.sv_shishkina.testapp65gb.di.components;

import dagger.Component;
import dagger.Subcomponent;
import ru.sv_shishkina.testapp65gb.di.modules.OperationsModule;
import ru.sv_shishkina.testapp65gb.di.scopes.ViewScope;
import ru.sv_shishkina.testapp65gb.ui.activity.MainActivity;
import ru.sv_shishkina.testapp65gb.ui.screen.employeedetails.EmployeeDetailsFragment;
import ru.sv_shishkina.testapp65gb.ui.screen.employees.EmployeesFragment;
import ru.sv_shishkina.testapp65gb.ui.screen.specialties.SpecialtiesFragment;

@ViewScope
@Subcomponent(modules = {
        OperationsModule.class
})
public interface ViewComponent {
    void inject(SpecialtiesFragment specialtiesFragment);
    void inject(EmployeesFragment employeesFragment);
    void inject(EmployeeDetailsFragment employeeDetailsFragment);
}
