package ru.sv_shishkina.testapp65gb.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.sv_shishkina.testapp65gb.di.scopes.ActivityScope;
import ru.sv_shishkina.testapp65gb.utils.navigator.NavigationHandler;

@Module
public class ActivityModule {
    final private NavigationHandler navigationHandler;

    public ActivityModule(NavigationHandler navigationHandler) {
        this.navigationHandler = navigationHandler;
    }

    @Provides
    @ActivityScope
    NavigationHandler provideNavigationHandler(){
        return navigationHandler;
    }
}
