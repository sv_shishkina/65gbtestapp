package ru.sv_shishkina.testapp65gb.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.DBDataSource;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.NetworkDataSource;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepository;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepositoryImpl;

@Module
public class RepositoryModule {
    @Provides
    @Singleton
    EmployeesRepository provideEmployeesRepository(NetworkDataSource networkDataSource, DBDataSource dbDataSource) {
        return new EmployeesRepositoryImpl(networkDataSource, dbDataSource);
    }
}
