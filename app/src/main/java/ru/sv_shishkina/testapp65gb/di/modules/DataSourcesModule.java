package ru.sv_shishkina.testapp65gb.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.DBDataSource;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.DBDataSourceImpl;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers.Mapper;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBEmployee;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBSpecialty;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DaoSession;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.NetworkDataSource;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.NetworkDataSourceImpl;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.Service65Gb;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

@Module
public class DataSourcesModule {
    @Provides
    @Singleton
    NetworkDataSource provideNetworkDataSource(Service65Gb service) {
        return new NetworkDataSourceImpl(service);
    }

    @Provides
    @Singleton
    DBDataSource provideDBDataSource(DaoSession daoSession, Mapper<Employee, DBEmployee> employeeMapper, Mapper<Specialty, DBSpecialty> specialtyMapper) {
        return new DBDataSourceImpl(daoSession, employeeMapper, specialtyMapper);
    }
}
