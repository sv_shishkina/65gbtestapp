package ru.sv_shishkina.testapp65gb.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepository;
import ru.sv_shishkina.testapp65gb.ui.screen.employeedetails.GetEmployeeOperation;
import ru.sv_shishkina.testapp65gb.ui.screen.employees.GetEmployeesOperation;
import ru.sv_shishkina.testapp65gb.ui.screen.specialties.GetSpecialtiesOperation;

@Module
public class OperationsModule {
    @Provides
    GetSpecialtiesOperation provideGetSpecialtiesOperation(EmployeesRepository repository) {
        return new GetSpecialtiesOperation(repository);
    }

    @Provides
    GetEmployeesOperation provideGetEmployeesOperation(EmployeesRepository repository) {
        return new GetEmployeesOperation(repository);
    }

    @Provides
    GetEmployeeOperation provideGetEmployeeOperation(EmployeesRepository repository) {
        return new GetEmployeeOperation(repository);
    }
}
