package ru.sv_shishkina.testapp65gb.di.components;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.sv_shishkina.testapp65gb.di.modules.ActivityModule;
import ru.sv_shishkina.testapp65gb.di.modules.ApplicationModule;
import ru.sv_shishkina.testapp65gb.di.modules.DBMappersModule;
import ru.sv_shishkina.testapp65gb.di.modules.DBModule;
import ru.sv_shishkina.testapp65gb.di.modules.DataSourcesModule;
import ru.sv_shishkina.testapp65gb.di.modules.NetworkModule;
import ru.sv_shishkina.testapp65gb.di.modules.RepositoryModule;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.DBDataSource;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.NetworkDataSource;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepository;
import ru.sv_shishkina.testapp65gb.ui.BaseActivity;
import ru.sv_shishkina.testapp65gb.ui.BaseFragment;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DataSourcesModule.class,
        NetworkModule.class,
        RepositoryModule.class,
        DBMappersModule.class,
        DBModule.class
})
public interface ApplicationComponent {
    Context context();

    EmployeesRepository employeesRepository();

    NetworkDataSource networkDataSource();

    DBDataSource dbDataSource();

    ActivityComponent plus(ActivityModule activityModule);
}
