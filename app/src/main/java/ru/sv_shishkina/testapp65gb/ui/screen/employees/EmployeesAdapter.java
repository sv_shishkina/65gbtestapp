package ru.sv_shishkina.testapp65gb.ui.screen.employees;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;
import ru.sv_shishkina.testapp65gb.ui.interfaces.OnItemClickListener;
import ru.sv_shishkina.testapp65gb.utils.Tools;

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeesAdapter.EmployeesViewHolder> {

    private List<Employee> data = new ArrayList<>();
    final private OnItemClickListener onItemClickListener;

    public EmployeesAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public EmployeesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmployeesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employee, parent, false));
    }

    @Override
    public void onBindViewHolder(EmployeesViewHolder holder, int position) {
        holder.onBind();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Employee> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    class EmployeesViewHolder extends RecyclerView.ViewHolder{
        @BindString(R.string.name_template)
        String nameTemplate;
        @BindString(R.string.age_template)
        String ageTemplate;

        @BindView(R.id.sdv_photo)
        SimpleDraweeView sdvPhoto;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_age)
        TextView tvAge;

        Employee employee;

        EmployeesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onClick(getAdapterPosition());
                }
            });
        }

        void onBind(){
            employee = data.get(getAdapterPosition());

            sdvPhoto.setImageURI(employee.getPhoto());
            String name = Tools.normalizeName(nameTemplate, employee.getFirstName(), employee.getLastName());
            tvName.setText(name);

            String age;
            if (employee.getBirthday() != null && !employee.getBirthday().isEmpty()) {
                age = String.format(ageTemplate, Tools.getAge(employee.getBirthday()));
            } else {
                age = String.format(ageTemplate, "-");
            }
            tvAge.setText(age);
        }
    }
}
