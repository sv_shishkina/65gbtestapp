package ru.sv_shishkina.testapp65gb.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.di.components.ApplicationComponent;
import ru.sv_shishkina.testapp65gb.ui.BaseActivity;
import ru.sv_shishkina.testapp65gb.ui.screen.specialties.SpecialtiesFragment;

public class MainActivity extends BaseActivity {
    private Unbinder viewBinder;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewBinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fl_content, SpecialtiesFragment.newInstance()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        if (viewBinder != null) {
            viewBinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void setupComponent(ApplicationComponent applicationComponent) {

    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
