package ru.sv_shishkina.testapp65gb.ui.interfaces;


public interface OnItemClickListener {
    void onClick(int adapterPosition);
}
