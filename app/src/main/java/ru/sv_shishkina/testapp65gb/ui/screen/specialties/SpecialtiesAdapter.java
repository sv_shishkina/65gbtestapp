package ru.sv_shishkina.testapp65gb.ui.screen.specialties;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;
import ru.sv_shishkina.testapp65gb.ui.interfaces.OnItemClickListener;

public class SpecialtiesAdapter extends RecyclerView.Adapter<SpecialtiesAdapter.SpecialtyViewHolder> {

    private List<Specialty> data = new ArrayList<>();
    final private OnItemClickListener onItemClickListener;

    public SpecialtiesAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public SpecialtyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SpecialtyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specialty, parent, false));
    }

    @Override
    public void onBindViewHolder(SpecialtyViewHolder holder, int position) {
        holder.onBind();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Specialty> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    class SpecialtyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_specialty)
        TextView tvSpecialty;

        Specialty specialty;

        SpecialtyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onClick(getAdapterPosition());
                }
            });
        }

        void onBind(){
            specialty = data.get(getAdapterPosition());
            tvSpecialty.setText(specialty.getTitle());
        }
    }
}
