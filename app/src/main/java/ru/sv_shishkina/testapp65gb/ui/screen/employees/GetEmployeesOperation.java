package ru.sv_shishkina.testapp65gb.ui.screen.employees;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import java.util.Collections;
import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.OperationResponse;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepository;

public class GetEmployeesOperation extends ChronosOperation<OperationResponse<List<Employee>>> {
    final private EmployeesRepository employeesRepository;

    private Long specialtyId;

    public GetEmployeesOperation(EmployeesRepository employeesRepository) {
        this.employeesRepository = employeesRepository;
    }

    @Nullable
    @Override
    public OperationResponse<List<Employee>> run() {
        final OperationResponse<List<Employee>> response = new OperationResponse<>();
        try {
            response.setSuccessful(true);
            response.setResult(employeesRepository.getEmployees(specialtyId));
            response.setError(null);
        } catch (Throwable e) {
            response.setSuccessful(false);
            response.setResult(Collections.<Employee>emptyList());
            response.setError(e);
        }

        return response;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<OperationResponse<List<Employee>>>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<OperationResponse<List<Employee>>> {
    }

    public void setSpecialtyId(Long specialtyId) {
        this.specialtyId = specialtyId;
    }
}
