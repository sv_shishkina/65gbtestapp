package ru.sv_shishkina.testapp65gb.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.redmadrobot.chronos.gui.fragment.ChronosSupportFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;
import ru.sv_shishkina.testapp65gb.App;
import ru.sv_shishkina.testapp65gb.di.components.ActivityComponent;
import ru.sv_shishkina.testapp65gb.di.components.ApplicationComponent;

public abstract class BaseFragment extends ChronosSupportFragment {
    private Unbinder viewBinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
        setupComponent(getActivityComponent());

        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewBinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        if (viewBinder != null) {
            viewBinder.unbind();
        }

        super.onDestroyView();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    protected ActivityComponent getActivityComponent() {
        return ((BaseActivity) getActivity()).getActivityComponent();
    }

    public abstract void setupComponent(ActivityComponent activityComponent);
}
