package ru.sv_shishkina.testapp65gb.ui.screen.specialties;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import icepick.State;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.di.components.ActivityComponent;
import ru.sv_shishkina.testapp65gb.di.modules.OperationsModule;
import ru.sv_shishkina.testapp65gb.domain.OperationResponse;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;
import ru.sv_shishkina.testapp65gb.ui.BaseFragment;
import ru.sv_shishkina.testapp65gb.ui.interfaces.OnItemClickListener;
import ru.sv_shishkina.testapp65gb.ui.screen.employees.EmployeesFragment;
import ru.sv_shishkina.testapp65gb.utils.BaseListBundler;
import ru.sv_shishkina.testapp65gb.utils.navigator.NavigationHandler;

public class SpecialtiesFragment extends BaseFragment {
    final public static String TAG = "SpecialtiesFragment";
    @Inject
    GetSpecialtiesOperation getEmployeesOperation;
    @Inject
    NavigationHandler navigationHandler;

    @BindView(R.id.rv)
    RecyclerView rvSpecialties;
    @BindView(R.id.pb_progress)
    MaterialProgressBar pb;

    @State
    Boolean isLoading;
    @State(value = BaseListBundler.class)
    List<Specialty> specialties;

    private SpecialtiesAdapter specialtiesAdapter;

    public SpecialtiesFragment() {
        // Required empty public constructor
    }

    public static SpecialtiesFragment newInstance() {
       return new SpecialtiesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            showLoading(isLoading = true);
            runOperation(getEmployeesOperation, TAG);
        }

        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeViewState();
    }

    @Override
    public void setupComponent(ActivityComponent activityComponent) {
        getActivityComponent().plus(new OperationsModule()).inject(this);
    }

    private void initViews() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.text_specialties);
        }

        specialtiesAdapter = new SpecialtiesAdapter(new OnItemClickListener() {
            @Override
            public void onClick(int adapterPosition) {
                Long id = specialties.get(adapterPosition).getId();
                String title = specialties.get(adapterPosition).getTitle();

                navigationHandler.replaceWithBackStack(EmployeesFragment.newInstance(id, title), null);
            }
        });

        rvSpecialties.setLayoutManager(new LinearLayoutManager(getContext()));
        rvSpecialties.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rvSpecialties.setAdapter(specialtiesAdapter);
    }

    private void resumeViewState() {
        showLoading(isLoading);

        if (specialties != null) {
            specialtiesAdapter.setData(specialties);
        }
    }

    private void showLoading(Boolean isLoading) {
        if (isLoading) {
            pb.setVisibility(View.VISIBLE);
        } else {
            pb.setVisibility(View.GONE);
        }
    }

    public void onOperationFinished(final GetSpecialtiesOperation.Result result) {
        showLoading(isLoading = false);
        if (!result.isSuccessful()) {
            return;
        }

        final OperationResponse<List<Specialty>> response = result.getOutput();

        if (!response.getSuccessful()) {
            return;
        }

        final List<Specialty> specialties = response.getResult();

        if (specialties.isEmpty()) {
            return;
        }

        this.specialties = specialties;
        specialtiesAdapter.setData(specialties);
    }
}
