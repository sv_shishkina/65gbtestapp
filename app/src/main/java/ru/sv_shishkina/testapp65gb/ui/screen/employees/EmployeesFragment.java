package ru.sv_shishkina.testapp65gb.ui.screen.employees;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import icepick.State;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.di.components.ActivityComponent;
import ru.sv_shishkina.testapp65gb.di.modules.OperationsModule;
import ru.sv_shishkina.testapp65gb.domain.OperationResponse;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.ui.BaseFragment;
import ru.sv_shishkina.testapp65gb.ui.activity.MainActivity;
import ru.sv_shishkina.testapp65gb.ui.interfaces.OnItemClickListener;
import ru.sv_shishkina.testapp65gb.ui.screen.employeedetails.EmployeeDetailsFragment;
import ru.sv_shishkina.testapp65gb.utils.BaseListBundler;
import ru.sv_shishkina.testapp65gb.utils.navigator.NavigationHandler;

@FragmentWithArgs
public class EmployeesFragment extends BaseFragment {
    final public static String TAG = "EmployeesFragment";

    @Inject
    GetEmployeesOperation getEmployeesOperation;
    @Inject
    NavigationHandler navigationHandler;

    @BindView(R.id.rv)
    RecyclerView rvEmployees;

    @Arg
    Long specialtyId;
    @Arg
    String title;

    @State(value = BaseListBundler.class)
    List<Employee> employees;

    private EmployeesAdapter employeesAdapter;
    private Toolbar toolbar;

    public EmployeesFragment() {
        // Required empty public constructor
    }

    public static EmployeesFragment newInstance(Long specialtyId, String title) {
        return EmployeesFragmentBuilder.newEmployeesFragment(specialtyId, title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            getEmployeesOperation.setSpecialtyId(specialtyId);
            runOperation(getEmployeesOperation, TAG);
        }

        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (employees != null) {
            employeesAdapter.setData(employees);
        }
    }

    @Override
    public void onStop() {
        toolbar.setNavigationIcon(null);
        toolbar.setNavigationOnClickListener(null);
        super.onStop();
    }

    private void initViews() {
        toolbar = ((MainActivity) getActivity()).getToolbar();
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.mipmap.arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });

        employeesAdapter = new EmployeesAdapter(new OnItemClickListener() {
            @Override
            public void onClick(int adapterPosition) {
                Long id = employees.get(adapterPosition).getId();
                navigationHandler.replaceWithBackStack(EmployeeDetailsFragment.newInstance(id), null);
            }
        });

        rvEmployees.setLayoutManager(new LinearLayoutManager(getContext()));
        rvEmployees.setAdapter(employeesAdapter);
    }

    @Override
    public void setupComponent(ActivityComponent activityComponent) {
        getActivityComponent().plus(new OperationsModule()).inject(this);
    }

    public void onOperationFinished(final GetEmployeesOperation.Result result) {
        if (!result.isSuccessful()) {
            return;
        }

        final OperationResponse<List<Employee>> response = result.getOutput();

        if (!response.getSuccessful()) {
            return;
        }

        final List<Employee> employees = response.getResult();

        if (employees.isEmpty()) {
            return;
        }

        this.employees = employees;
        employeesAdapter.setData(employees);
    }
}
