package ru.sv_shishkina.testapp65gb.ui.screen.employeedetails;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import java.util.Collections;
import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.OperationResponse;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepository;

public class GetEmployeeOperation extends ChronosOperation<OperationResponse<Employee>> {
    final private EmployeesRepository employeesRepository;

    private Long employeeId;

    public GetEmployeeOperation(EmployeesRepository employeesRepository) {
        this.employeesRepository = employeesRepository;
    }

    @Nullable
    @Override
    public OperationResponse<Employee> run() {
        final OperationResponse<Employee> response = new OperationResponse<>();
        try {
            response.setSuccessful(true);
            response.setResult(employeesRepository.getEmployee(employeeId));
            response.setError(null);
        } catch (Throwable e) {
            response.setSuccessful(false);
            response.setResult(null);
            response.setError(e);
        }

        return response;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<OperationResponse<Employee>>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<OperationResponse<Employee>> {
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
}
