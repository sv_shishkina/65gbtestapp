package ru.sv_shishkina.testapp65gb;


import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

import ru.sv_shishkina.testapp65gb.di.components.ApplicationComponent;
import ru.sv_shishkina.testapp65gb.di.components.DaggerApplicationComponent;
import ru.sv_shishkina.testapp65gb.di.modules.ApplicationModule;
import timber.log.Timber;

public class App extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        setUpComponent();
    }

    private void setUpComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
    public static App from(Context context) {
        return (App) context.getApplicationContext();
    }
}
